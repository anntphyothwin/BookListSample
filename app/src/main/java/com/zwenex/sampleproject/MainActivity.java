package com.zwenex.sampleproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements BookRecyclerViewAdapter.BookClickListener {

    private ArrayList<Book> bookList = new ArrayList<>();
    private BookRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bookList.add(new Book("Book A", "Author A", 50));
        bookList.add(new Book("Book B", "Author B", 51));
        bookList.add(new Book("Book C", "Author C", 52));
        bookList.add(new Book("Book D", "Author D", 53));

        RecyclerView bookRecyclerView = findViewById(R.id.book_recycler_view);
        bookRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new BookRecyclerViewAdapter();
        adapter.setBookClickListener(this);
        bookRecyclerView.setAdapter(adapter);
        adapter.refreshList(bookList);
    }

    @Override
    public void onBookClick(Book book) {
        Intent intent = new Intent(this, BookDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("book", book);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
