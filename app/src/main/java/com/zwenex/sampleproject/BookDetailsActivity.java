package com.zwenex.sampleproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class BookDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        TextView bookTitle = findViewById(R.id.book_title);
        TextView bookAuthor = findViewById(R.id.book_author);
        TextView bookPages = findViewById(R.id.book_pages);

        Bundle bundle = getIntent().getExtras();
        Book book = (Book)bundle.getSerializable("book");
        Log.e("BUNDLEEE", book.toString());
        bookTitle.setText(book.getTitle());
        bookAuthor.setText(book.getAuthor());
        bookPages.setText(String.valueOf(book.getPages()));
    }
}
